package pl.tim3erland.blockchain.simple.dto;


import lombok.Data;
import pl.tim3erland.blockchain.simple.HashUtils;

import java.time.LocalDateTime;

@Data
public class BlockDto {
    private String data;
    private LocalDateTime blockTime;
    private String hash;
    private String previosBlockHash;

    public BlockDto() {
    }

    public BlockDto(String data, LocalDateTime blockTime, String previosBlockHash) {
        this.data = data;
        this.blockTime = blockTime;
        this.previosBlockHash = previosBlockHash;
        this.hash = HashUtils.hashBlock(this);
    }
}
