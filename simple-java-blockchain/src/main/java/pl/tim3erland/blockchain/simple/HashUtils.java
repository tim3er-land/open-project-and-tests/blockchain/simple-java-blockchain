package pl.tim3erland.blockchain.simple;

import com.google.common.hash.Hashing;
import pl.tim3erland.blockchain.simple.dto.BlockDto;

import java.nio.charset.StandardCharsets;

public class HashUtils {
    public static String hashBlock(BlockDto blockDto) {
        return Hashing.sha256()
                .hashString(getBlock(blockDto), StandardCharsets.UTF_8)
                .toString();
    }

    private static String getBlock(BlockDto blockDto) {
        return blockDto.getPreviosBlockHash() + blockDto.getPreviosBlockHash() + blockDto.getBlockTime() + blockDto.getData();
    }
}
