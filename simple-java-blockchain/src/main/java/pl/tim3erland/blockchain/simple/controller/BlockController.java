package pl.tim3erland.blockchain.simple.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.tim3erland.blockchain.simple.dto.BlockDto;
import pl.tim3erland.blockchain.simple.service.BlockService;

import java.util.List;

@Log4j2
@RestController
public class BlockController {
    @Autowired
    private BlockService blockService;

    @PutMapping("/block")
    public void addNewBlock(@RequestBody String data) {
        blockService.addNewBlock(data);
    }

    @GetMapping("/block")
    public ResponseEntity<List<BlockDto>> getAllBlocks() {
        List<BlockDto> blockDtos = blockService.getAllBLocks();
        return ResponseEntity.ok(blockDtos);
    }

    @GetMapping("/block/{hash}")
    public ResponseEntity<BlockDto> getBlockByHash(@PathVariable("hash") String hash) {
        BlockDto blockDto = blockService.getBlockByHash(hash);
        return ResponseEntity.ok(blockDto);
    }
}
