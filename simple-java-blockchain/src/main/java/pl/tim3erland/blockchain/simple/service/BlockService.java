package pl.tim3erland.blockchain.simple.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.tim3erland.blockchain.simple.dto.BlockDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class BlockService {

    List<BlockDto> blockDtos;

    public BlockService() {
        this.blockDtos = new ArrayList<>();
    }

    public void addNewBlock(String data) {
        String previosHash = "";
        if (!blockDtos.isEmpty()) {
            previosHash = blockDtos.get(blockDtos.size() - 1).getHash();
        }
        log.info("Start prepare block with data: {}", data);
        BlockDto e = new BlockDto(data, LocalDateTime.now(), previosHash);
        log.info("End prepare block with data: {} and hash: {}", data, e.getHash());
        blockDtos.add(e);
    }

    public List<BlockDto> getAllBLocks() {
        log.info("Return all block. Find {} block", blockDtos.size());
        return blockDtos;
    }

    public BlockDto getBlockByHash(String hash) {
        log.info("Searching block with hash: {}", hash);
        return blockDtos.stream().filter(blockDto -> blockDto.getHash().equalsIgnoreCase(hash)).findFirst().orElse(null);
    }
}
