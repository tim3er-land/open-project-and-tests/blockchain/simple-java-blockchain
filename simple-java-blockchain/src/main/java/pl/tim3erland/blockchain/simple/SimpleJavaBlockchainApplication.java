package pl.tim3erland.blockchain.simple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.tim3erland.blockchain.simple.dto.BlockDto;
import pl.tim3erland.blockchain.simple.service.BlockService;

import java.util.Random;

@SpringBootApplication
public class SimpleJavaBlockchainApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleJavaBlockchainApplication.class, args);
    }
}
